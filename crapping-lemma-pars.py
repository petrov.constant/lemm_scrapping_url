#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr 18 18:29:11 2019

@author: Konstantin Petrov

Лемматизация текста по указанным URL

"""

import urllib
from bs4 import BeautifulSoup
from collections import Counter,OrderedDict
import pymorphy2
import pandas as pd





sites=['https://mail.ru','https://yandex.ru']


def text_from_sites(url):
    try:
        html = urllib.request.urlopen(url).read()
    except Exception:
        return ''
    soup = BeautifulSoup(html, "html.parser" )
    
    # kill all script and style elements
    for script in soup(["script", "style"]):
        script.extract()    # rip it out

    text = soup.get_text(separator=" ")
    # break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())
    # break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))
    # drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)
    return text+' '





morph = pymorphy2.MorphAnalyzer()
text_top=[]
for i in sites:
    text=text_from_sites(i).strip().lower()
    text=' '.join(str(l) for l in text.splitlines())
    top_lemm=[]
    for word in text.split():
        top_lemm.append(morph.normal_forms(word)[0])
    text=' '.join(str(l) for l in top_lemm)
    text_top.append(text)
    


text_all=' '.join(str(l) for l in text_top)
text_lemm=Counter(text_all.split())
drop_keys=[]
drop_values=[]
for key,value in text_lemm.items():
    if len(key)<4 or value<1:
        drop_keys.append(key)
for key in drop_keys:
    text_lemm.pop(key)


columns=['top_lemm','all']
for i in range(len(sites)):
    #columns.append(str(i))
    columns.append(sites[i])

df=pd.DataFrame(columns=columns)
l=0
for lemm in text_lemm:
    line=[lemm]
    line.append(len([i for i in text_all.split() if i==lemm]))
    for s in range(len(sites)):
        line.append(len([i for i in text_top[s].split() if i==lemm]))
    df.loc[l]=line
    l+=1

df.sort_values(by=['all'], ascending=False, inplace=True)        
df.to_excel('top_10_lemmas.xlsx',encoding='UTF-8')
        
    


